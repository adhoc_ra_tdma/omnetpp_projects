#include <ratdma/unicast_sender.hpp>


#include <debug_headers/clear_debug.hpp>

//#define debugLevel lDebug
#ifndef debugLevel
#define debugLevel lLog
#endif

#include <debug_headers/debug.hpp>


UnicastSender::UnicastSender(ipv4_address addr, milliseconds period, uint16_t port, std::function<void(const upd_message &)> userUnicastHandler):
	ip(addr),
	buffer(addr.getBytes()[3]),
	port(port),
	userUnicastHandler(userUnicastHandler),
	r(addr.getBytes()[3])
{
	socket=common::simulation_interface::udp_sockets::new_udp_unicast_socket(port);
	next_send=common::simulation_interface::timer::new_timer();
	r.setPeriod(period);
	socket->start();
	socket->async_read([this](const upd_message &msg)
	{
		// we need to change it
		upd_message copy_of_msg(msg);

		//do smth
		uint8_t source = copy_of_msg.getData()[0];
		uint8_t destination = copy_of_msg.getData()[1];

		PDEBUG(std::cout,"Received a message from "<<+source<<" to "<<+destination);
		if(destination==ip.getBytes()[3])
		{
			//its for me
			PINFO(std::cout,"I received a new message!");
			copy_of_msg.getData().erase(copy_of_msg.getData().begin());
			copy_of_msg.getData().erase(copy_of_msg.getData().begin());
			copy_of_msg.getData().erase(copy_of_msg.getData().begin());
			this->userUnicastHandler(copy_of_msg);
		}
		else
		{
			std::list<std::size_t> path;
			bool fits=r.getFeasableRoute(path, source, destination,copy_of_msg.getData().size());
			if(fits)
			{
				ipv4_address destination_address(ip);
				std::array<uint8_t, 4> bytes = destination_address.getBytes();

				// Should never happen
				assert(path.size()>0);

				bytes[3]=path.front();

				destination_address.setBytes(bytes);
				udp_endpoint dest_endp(destination_address.getAddr(),this->port);
				copy_of_msg.setUDPEndpoint(dest_endp);
				socket->write(copy_of_msg);
			}
			else
			{
				shared_buffers::message_t msg_to_keep;
				copy_of_msg.getData().erase(copy_of_msg.getData().begin());
				copy_of_msg.getData().erase(copy_of_msg.getData().begin());
				copy_of_msg.getData().erase(copy_of_msg.getData().begin());
				std::copy(copy_of_msg.getData().begin(), copy_of_msg.getData().end(), msg_to_keep.msg.begin());
				msg_to_keep.size=copy_of_msg.getData().size();
				msg_to_keep.destination=destination;
				buffer.push_message(msg_to_keep);
			}
		}
	});
}

bool UnicastSender::start_slot(const upd_message &msg, const time_point &start_of_slot, const matrix_t & connectivity_matrix, const buffer_state_map &buffer_state)
{

	r.setStartOfSlot(start_of_slot);
	r.updateRoutes(connectivity_matrix,buffer_state);

	if(buffer.size()>0)
	{
		next_send->restart([this](){process_next_packet();}, std::chrono::duration_cast<duration>(duration_double(T_MULTICAST(msg.getData().size()+IP_UDP,BROADCAST_DATARATE))), duration::zero());
		return true;
	}
	return false;

}

void UnicastSender::update_table(const matrix_t &new_connectivity_matrix, const buffer_state_map &buffer_state)
{
	r.updateRoutes(new_connectivity_matrix,buffer_state);
}

void UnicastSender::process_next_packet()
{
	// Get next from buffer
	shared_buffers::message_t packet;
	upd_message msg;
	//if any
	PDEBUG(std::cout,"Fetching message...");
	if(get_next_routable_packet(packet))
	{
		PINFO(std::cout,"Fetched message to "<<+packet.destination<<" size "<<packet.size<<"B");
		//if fits
		std::list<std::size_t> path;
		bool fits=r.getFeasableRoute(path,ip.getBytes()[3], packet.destination,packet.size+3);
		PDEBUG(std::cout,"Fitting message...");
		if(fits)
		{
			PDEBUG(std::cout,"It fits...");
			upd_message::pType data;
			data.push_back(ip.getBytes()[3]);
			data.push_back(packet.destination);
			data.push_back(packet.priority);
			data.insert(data.end(),packet.msg.begin(),packet.msg.begin()+packet.size);
			msg.setData(data);

			ipv4_address destination_address(ip);
			std::array<uint8_t, 4> bytes = destination_address.getBytes();
			PDEBUG(std::cout,"My IP "<<ip.getAddr()<<" "<<[&](){
				std::stringstream ss;
				for(uint8_t &b:bytes)
				{
					ss<<+b<<".";
				}
				return ss.str();
			}());
			// Should never happen
			assert(path.size()>0);
			bytes[3]=path.front();
			PDEBUG(std::cout,"Sending message to "<<[&bytes](){
				std::stringstream ss;
				for(uint8_t &b:bytes)
				{
					ss<<+b<<".";
				}
				return ss.str();
			}());
			destination_address.setBytes(bytes);
			udp_endpoint dest_endp(destination_address.getAddr(),port);
			msg.setUDPEndpoint(dest_endp);
			PDEBUG(std::cout,"Sending message...");
			socket->write(msg);
			next_send->restart([this](){process_next_packet();}, std::chrono::duration_cast<duration>(duration_double(path.size()*T_UNICAST(msg.getData().size()+IP_UDP,DATARATE))), duration::zero());
			buffer.get_message(packet);
		}
		//set timer

	}

}

bool UnicastSender::get_next_routable_packet(common::buffers::shared_buffers::message_t &packet)
{
	std::size_t buffer_not_checked=buffer.size();
	bool found_packet=false;
	bool done=false;
	while( (!done) && (buffer_not_checked>0) )
	{
		if(!buffer.peek_message(packet))
		{
			done=true;
		}
		else if(r.hasRoute(packet.destination))
		{
			found_packet=true;
			done=true;
		}
		else
		{
			buffer.get_message(packet);
			buffer.push_message(packet);
			buffer_not_checked--;
		}
	}
	return found_packet;

}


std::string UnicastSender::get_routing_string() const
{
	return r.toString();
}

const Router &UnicastSender::getRouter() const
{
	return r;
}
