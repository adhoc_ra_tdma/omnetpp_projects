#include <ratdma/raTDMA.hpp>


#include <debug_headers/clear_debug.hpp>

//#define debugLevel lInfo
#ifndef debugLevel
#define debugLevel lLog
#endif

#include <debug_headers/debug.hpp>

#include <map>

namespace communications
{
namespace raTDMA
{
raTDMA::raTDMA(network_device_i &dev,
			   uint64_t setPeriod,
			   uint64_t min,
			   uint64_t max,
			   std::function<void(std::vector<uint8_t> &, const buffer_state_map &, const matrix_t &matrix, bool &, bool &)> sendFunction,
			   std::function<void(const udp_message &, const buffer_state_map &)> rcvFunction,
			   std::function<void(const udp_message &)> userUnicastHandler):

	myAddress(dev.getAddr()),
	current_cycle_start(clock_t::now()),
	period(std::chrono::milliseconds(setPeriod)),
	minDelay(std::chrono::milliseconds(min)),
	maxDelay(std::chrono::milliseconds(max)),
	periodDelay(std::chrono::nanoseconds::zero()),
	configured(false),
	#ifdef WAIT_FOR_ONE
	lock(0),
	#endif
	conMatrix(myAddress.getBytes()[3], std::chrono::milliseconds(10*setPeriod)),
	sendingTimeLog("./sendingTimeLog"+myAddress.getAddr()+".log"),
	userTimerHandler(sendFunction),
	userReceiveDone(rcvFunction),
	unicast_sender(myAddress,std::chrono::milliseconds(setPeriod),UNICAST_PORT,userUnicastHandler)
{
	mcast=common::simulation_interface::udp_sockets::new_udp_multicast_socket(dev.getName(),udp_endpoint_t(MULTICAST_IP, MULTICAST_PORT)),
	mcast->async_read([this](const udp_message &msg){this->receiveDone(msg);});


    sendTimer = common::simulation_interface::timer::new_timer();

#ifdef WAIT_FOR_ONE
	if(myAddress!=address::from_string("10.0.0.1")) lock.wait();
#endif

	sendTimer->start([this](){this->fired();},period,period);
	mcast->start();
	std::cout<< "Started communications: " << myAddress.getAddr() << "="<<
				(uint16_t)myAddress.getBytes()[0]<<"."<<(uint16_t)myAddress.getBytes()[1]<<"."<<(uint16_t)myAddress.getBytes()[2]<<"."<<(uint16_t)myAddress.getBytes()[3]<<"="<<
				myAddress.getInt()<<std::endl<<
				"Period = "<<setPeriod<<" Min = "<<min<<" Max = "<<max<<std::endl;



	configured=true;
}

raTDMA::~raTDMA()
{

}

void raTDMA::synchronise(ipv4_address receivedAddress, time_point estimated_transmission_time)
{
	if(!configured) return;
	PDEBUG(std::cout, "ID "<<myAddress.getAddr());

	PDEBUG(std::cout, "Team "<<conMatrix);
	PDEBUG(std::cout, "Received "<<receivedAddress.getAddr()<< " at " << estimated_transmission_time.time_since_epoch().count());

	std::size_t numberOfSlots=conMatrix.getNumberOfMembers();

	double double_period=std::chrono::duration_cast<duration_double>(period).count();
	duration_double double_slot_duration=duration_double(double_period/conMatrix.getNumberOfMembers());
	duration slot_duration=std::chrono::duration_cast<duration>(double_slot_duration);

	time_point begining_of_slot_after_rx;
	// This can happen if message was sent at roughly the same time as mine!
	// If message estimated send time is before my slot
	if( (estimated_transmission_time<current_cycle_start) )
	{
		// It was sent "before" me
		begining_of_slot_after_rx= time_point(sendTimer->absoluteExpiryTime())-period;
		PDEBUG(std::cout,"Was sent before my slot: "<<current_cycle_start.time_since_epoch().count());
	}
	else
	{
		begining_of_slot_after_rx=time_point(sendTimer->absoluteExpiryTime());
	}
	PDEBUG(std::cout,"Next time to send: "<<begining_of_slot_after_rx.time_since_epoch().count());
	std::chrono::nanoseconds delay=std::chrono::nanoseconds::zero();

	uint8_t othersSlot;
	conMatrix.getLocalID(receivedAddress.getBytes()[3],othersSlot);
	uint8_t mySlot;
	conMatrix.getLocalID(myAddress.getBytes()[3],mySlot);

	int windowsUntilTransmission = ( numberOfSlots - othersSlot + mySlot ) % numberOfSlots;
	PDEBUG(std::cout, "Should send at " << begining_of_slot_after_rx.time_since_epoch().count()<<" (" << windowsUntilTransmission<<" windows left)");

	std::chrono::nanoseconds timeUntilTransmission = (period / numberOfSlots)*windowsUntilTransmission;
	PDEBUG(std::cout, "Time until transmission   " << timeUntilTransmission.count());

	time_point newTimeToSend = estimated_transmission_time+timeUntilTransmission;
	PDEBUG(std::cout, "Will send at   " << newTimeToSend.time_since_epoch().count());


	if(newTimeToSend>begining_of_slot_after_rx)
	{
		delay = newTimeToSend - begining_of_slot_after_rx;
		PDEBUG(std::cout, "Was delayed by "<<delay.count());
	}

	if(delay<minDelay)
	{
		PDEBUG(std::cout, "Too small");
		delay=std::chrono::nanoseconds::zero();
	}
	if(periodDelay+delay<=maxDelay)
	{
		PDEBUG(std::cout, "OK");
		periodDelay+=delay;
	}
	else
	{
		PDEBUG(std::cout, "Too Big: "<<delay.count()<<std::endl<<"Max delay: "<<maxDelay.count()<<" Period Delay: "<<periodDelay.count()<<" -->"<<(maxDelay-periodDelay).count());
		delay=maxDelay-periodDelay;
		periodDelay=maxDelay;
	}
	PINFO(std::cout, "Delay "<<delay.count());
	if(delay>std::chrono::nanoseconds::zero()) // if there is a delay
	{
		PDEBUG(std::cout, "Will delay");
		sendTimer->delay(delay);
	}
}

//    void raTDMA::cleanup()
//    {
//        // Connectivity only uses last byte so the begining does not matter
//        for(int i=0;i<MAX_AGENTS;i++)
//        {
//            std::stringstream ss;
//            ss << (i+1);
//            std::string str = ss.str();

//            if(conMatrix.isMember(boost::asio::ip::address(boost::asio::ip::address::from_string("10.0.0."+str))))
//            {
//                slots.addElement(boost::asio::ip::address::from_string("10.0.0."+str));
//            }
//            else
//            {
//                slots.removeElement(boost::asio::ip::address::from_string("10.0.0."+str));
//            }
//        }
//    }


void raTDMA::fired()
{
	std::chrono::high_resolution_clock::time_point time_start=std::chrono::high_resolution_clock::now();
	sending.lock();
	try
	{

		sendBuffer.clear();
		//ADD MATRIX DATA
		conMatrix.getPackedState(sendBuffer);
		//ASK DATA FROM USER
		std::vector<uint8_t> data;
		current_cycle_start=clock_t::now();

		bool active;
		bool end;

		userTimerHandler(data,conMatrix.get_buffer_state(), conMatrix.get_connectivity_matrix(), active, end);
		sendBuffer.insert(sendBuffer.end(),data.begin(),data.end());
		udp_message msg(std::string(MULTICAST_IP), MULTICAST_PORT,sendBuffer);



		bool must_transmit=unicast_sender.start_slot(msg,current_cycle_start,conMatrix.get_connectivity_matrix(), conMatrix.get_buffer_state());
		std::cout<<conMatrix.toString()<<std::endl;
		std::cout<<conMatrix.get_buffer_state().toString()<<std::endl;
		std::cout<<unicast_sender.get_routing_string()<<std::endl;
		//std::cout<<conMatrix.get_buffer_state().toString()<<std::endl;

		//SEND DATA
		periodDelay=std::chrono::nanoseconds::zero();
		if(must_transmit||active)
		{
			mcast->write(msg);
			if(active)
			{
				PLOG(sendingTimeLog,myAddress.getAddr()<<","<<std::chrono::duration_cast<std::chrono::milliseconds>(sendingTimeCounter->get_time()).count()<<" N");
				/*LOG_FUNCTION
				{
					std::cout<<myAddress.getAddr()<<","<<std::chrono::duration_cast<std::chrono::milliseconds>(sendingTimeCounter.getTime()).count()<<" N";
				};*/
			}
			else
			{
				PLOG(sendingTimeLog,myAddress.getAddr()<<","<<std::chrono::duration_cast<std::chrono::milliseconds>(sendingTimeCounter->get_time()).count()<<" F");
				/*LOG_FUNCTION
				{
					std::cout<<myAddress.getAddr()<<","<<std::chrono::duration_cast<std::chrono::milliseconds>(sendingTimeCounter.getTime()).count()<<" F";
				};*/
			}
			sendingTimeCounter->reset();
			//cleanup();
			PINFO(std::cout,"Sent: "<<msg.getData().size());
		}
		else
		{
			PLOG(sendingTimeLog,myAddress.getAddr()<<","<<std::chrono::duration_cast<std::chrono::milliseconds>(sendingTimeCounter->get_time()).count()<<" X");
			/*LOG_FUNCTION
			{
				std::cout<<myAddress.getAddr()<<","<<std::chrono::duration_cast<std::chrono::milliseconds>(sendingTimeCounter.getTime()).count()<<" X";
			};*/
			sendingTimeCounter->reset();
			//cleanup();
			PINFO(std::cout,"Inactive: "<<msg.getData().size());

		}

		if(end)
		{
			sendTimer->stop();
		}

	}
	catch (std::exception& e)
	{
		PERROR(std::cerr, "Exception: " << e.what());
	}
	sending.unlock();
	std::chrono::high_resolution_clock::time_point time_end=std::chrono::high_resolution_clock::now();
	//To avoid warning
	(time_end-time_start);
	PDEBUG(std::cout,"Timer: "<<(time_end-time_start).count());

}



void raTDMA::receiveDone(const udp_message &msg)
{
	std::chrono::high_resolution_clock::time_point time_start=std::chrono::high_resolution_clock::now();

	try{
		std::vector<uint8_t> buffer=msg.getData();
		std::size_t bytes_transferred=buffer.size();
		const udp_endpoint_i &source=msg.getUDPEndpoint();

		if(source.getBytes()[0]==127)
		{
			PWARNING(std::cout,"Received from loopback... Ignoring");
			return;
		}

		/*std::cout<< "ID "<<+myAddress.getBytes()[3]<<" received from "<<+msg.getUDPEndpoint().getBytes()[3]<<std::endl;
		for(matrix_t::value_type &row: conMatrix.getConnectivityMatrix())
		{
			if(row.first==myAddress.getBytes()[3])
			{
				for(matrix_t::mapped_type::value_type &col: row.second)
				{
					std::cout<<+col.second<<" ";
				}
			}
		}
		std::cout<<std::endl;
*/
		time_point now = clock_t::now();
		std::chrono::nanoseconds msg_tx_duration=std::chrono::duration_cast<std::chrono::nanoseconds>(duration_double(T_MULTICAST(bytes_transferred+IP_UDP,BROADCAST_DATARATE)));
		PLOG(sendingTimeLog,source.getAddr()<<","<<(std::chrono::duration_cast<std::chrono::milliseconds>(sendingTimeCounter->get_time()-msg_tx_duration).count()));
		sendingTimeCounter->reset(-msg_tx_duration);
		if(source.getInt()!=myAddress.getInt())
		{

			PINFO(std::cout,"Received ("<<bytes_transferred<<") from "<<source.getAddr());
#ifdef WAIT_FOR_ONE
			if(source.address()==address::from_string("10.0.0.1")) lock.post();
#endif
#ifdef SYNCHRONISED
			if(conMatrix.get_state().is_source(source.getBytes()[3]) && conMatrix.get_state().is_sink(source.getBytes()[3]) )
			{
				PDEBUG(std::cout,"Now "<<now.time_since_epoch().count());
				PDEBUG(std::cout,"Message duration "<<msg_tx_duration.count());
				PDEBUG(std::cout,"Diff "<<(now-msg_tx_duration).time_since_epoch().count());
				synchronise(source.getAddr(), now-msg_tx_duration);
			}
#endif
			conMatrix.pushPackedState(source.getBytes()[3], true, buffer);
			udp_message msg_clean(msg.getUDPEndpoint().getAddr(),msg.getUDPEndpoint().getPort(),buffer);
			userReceiveDone(msg_clean, conMatrix.get_buffer_state());

			unicast_sender.update_table(conMatrix.get_connectivity_matrix(), conMatrix.get_buffer_state());
		}
		else
		{
			PLOG(std::cout,"Received from myself");
		}
	}
	catch(std::exception &e)
	{
		std::cout<<"MY exception catch "<<e.what()<<std::endl;
		throw e;
	}
	std::chrono::high_resolution_clock::time_point time_end=std::chrono::high_resolution_clock::now();
	//To avoid warning
	(time_end-time_start);
	PDEBUG(std::cout,"Received: "<<(time_end-time_start).count());
}
}
}
