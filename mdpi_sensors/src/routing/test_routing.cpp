#ifdef COMPILE_ME
#include <routing/routing.hpp>
#include <dynamic_map_matrix/square_matrix.hpp>
#include <iostream>
#include <fstream>
#include <string>
#include <functional>
int main(int argc, char **argv)
{

	auto skip_chars=[](std::ifstream &myfile)
	{
		return;
		int c=myfile.peek();
		while(!std::isdigit(c))
		{
			myfile>>c;
			std::cout<<c;
			std::cin.get();
			c=myfile.peek();
		}
	};
	Router::matrix_t m;
	Router::buffer_state_map buffers;
	int number_of_nodes;

	int node_number;

	int element;

	int c;
	std::ifstream myfile (std::string(argv[1]), std::ios::in|std::ios::binary);
	if (myfile.is_open())
	{
		myfile>>number_of_nodes;
		std::cout<<"Got "<<number_of_nodes<<" nodes"<<std::endl;
		for(int i=0;i<number_of_nodes;i++)
		{
			myfile>>node_number;
			m.emplace(node_number, 0);
			buffers.emplace(node_number, 0);
			std::cout<<"Got "<<node_number<<std::endl;
			skip_chars(myfile);
		}
		skip_chars(myfile);
		for(Router::matrix_t::value_type &row:m)
		{
			for(Router::matrix_t::mapped_type::value_type &col:row.second)
			{
				myfile>>element;
				col.second=element;
				skip_chars(myfile);
			}
		}
		skip_chars(myfile);
		for(Router::buffer_state_map::value_type &pair:buffers)
		{
			myfile>>element;
			pair.second=element;
			skip_chars(myfile);
		}
		myfile.close();
	}


	std::cout<<m.toString()<<std::endl;
	std::cout<<buffers.toString()<<std::endl;
	for(int i=1;i<2;i++)
	{
		Router r(i);
		r.updateRoutes(m,buffers);
		std::cout<<r.toString()<<std::endl<<std::endl;
	}
	return 0;
}
#endif
