#include <routing/routing.hpp>
#include <algorithm>

#include <tuple>




//#define debugLevel lDebug
#ifndef debugLevel
#define debugLevel lError
#endif
#include <debug_headers/debug.hpp>


#define print_node ([](const node_t &node)\
{\
	std::stringstream ss;\
	ss<<"Distance: "<<(getDist(node))<<" Buffer: "<<(getBuffer(node))<<" Previous: "<<(getPrev(node))<<" visited: "<<(isVisited(node));\
	return ss.str();\
	})

#define print_map(map) for(const map_t::value_type &node:map) {std::cout<<print_node(node.second)<<std::endl;}



dijkstra::dijkstra(std::size_t myID):
	my_id(myID)
{

}

void dijkstra::push(const matrix_t &matrix, const buffer_state_map &buffers)
{
	solve_dijkstra(matrix,buffers,my_id);
}

void dijkstra::get_route_to(std::list<std::size_t> &route_to_dest, std::size_t dest) const
{
	const node_t &node=route.at(dest);
	const std::size_t &prev=getPrev(node);
	PDEBUG(std::cout,"Finding route to destination: "<<dest<<" node info: "<<print_node(node));
	if( prev != dest )
	{ //If we are not at the source node
		PDEBUG(std::cout,"Not at source... going to "<<prev);
		get_route_to(route_to_dest,getPrev(node));
	}
	route_to_dest.push_back(dest);
	PDEBUG(std::cout,"Done!");
}

dijkstra::map_t::value_type &dijkstra::minDistance(map_t &nodes)
{
	// Initialize min value
	map_t::iterator init=std::find_if(nodes.begin(),nodes.end(),[](map_t::value_type node){return !isVisited(node.second);});
	map_t::key_type min=init->first;
	PDEBUG(std::cout,"Initialising min to "<<+min<<" "<<print_node(nodes.at(min)));
	for (map_t::iterator it=init;it!=nodes.end();it++)
	{
		map_t::value_type &node=*it;
		PDEBUG(std::cout,"Visiting node "<<+node.first<<" "<<print_node(node.second));

		bool is_not_visited=(isVisited(node.second) == false);
		bool is_closer=(getDist(node.second) < getDist(nodes.at(min)));
		bool is_at_same_distance=(getDist(node.second) == getDist(nodes.at(min)));
		bool buffer_is_less_occupied=(getBuffer(node.second)<getBuffer(nodes.at(min)));

		PDEBUG(std::cout,std::boolalpha<<"Is not visited:"<<is_not_visited);
		PDEBUG(std::cout,std::boolalpha<<"Is closer:"<<is_closer);
		PDEBUG(std::cout,std::boolalpha<<"Is at same distance:"<<is_at_same_distance);
		PDEBUG(std::cout,std::boolalpha<<"Is less occupied:"<<buffer_is_less_occupied);

		if(	is_not_visited && ( is_closer || (is_at_same_distance && buffer_is_less_occupied ) ) )
		{
			min = node.first;
			PDEBUG(std::cout,"Setting min to "<<+min<<" "<<print_node(nodes.at(min)));
		}
	}


	//std::min_element();


	return *nodes.find(min);
}

std::string dijkstra::printPath() const
{
	std::stringstream ss;
	for(const map_t::value_type &node:route)
	{
		std::list<std::size_t> path;
		get_route_to(path,node.first);
		ss<<+node.first<<": ";
		std::list<std::size_t>::iterator elem=path.begin();
		for(std::size_t i=0;i<path.size(); i++)
		{
			ss<<+(*(elem++));
			if(i<(path.size()-1))
			{
				ss<<"->";
			}
		}
		ss<<std::endl;
	}
	return ss.str();
}

void dijkstra::solve_dijkstra(const matrix_t &graph, const buffer_state_map &buffers, int src)
{

	/** The output map. node_map.at(id) will hold:
	 *
	 *	get<0> = dist:
	 *		The shortest distance from src to id
	 *
	 *	get<1> = buffer_state:
	 *		For every node id other than the source, remember not only the
	 *		distance to it, but also the previous node in the path to it
	 *
	 *	get<2> = prev:
	 *		For every node id other than the source, remember not only the
	 *		distance to it, but also the previous node in the path to it
	 *
	 *	get<3> = visited
	 *		For every node id, if vertex id is included in shortest path tree
	 *		or shortest distance from src to id is finalized
	 */
	map_t node_map;

	/**
	 * Initialize all distances as INFINITE
	 * Initialize all buffer with buffer state
	 * Initialize all previous nodes as something impossible (self)
	 * Initialize all visited state to false
	 */
	for(const uint8_t &id: graph.getKeys())
	{
		node_t node(-1,buffers(id),id,false);
		node_map.emplace_hint(node_map.end(),id,node);
	}
	//print_map(node_map);

	// distance from source is zero
	getDist(node_map.at(src))=0;
	PDEBUG(std::cout, "Source is "<<+src<<":");
	// Find shortest path for all vertices
	for (std::size_t i=0;i<node_map.size();i++)
	{
		PDEBUG(std::cout, "Round "<<+i<<":");
		// Pick the minimum distance vertex from the set of vertices not
		// yet processed. u is always equal to src in first iteration.
		map_t::value_type &u = minDistance(node_map);
		PDEBUG(std::cout,"\tSelected node: "<<u.first<<" "<<print_node(u.second));

		// Mark the picked vertex as processed
		isVisited(u.second) = true;
		PDEBUG(std::cout,"\t\tMarking as visited: "<<u.first<<" "<<print_node(u.second));

		// Update dist value of the adjacent vertices of the picked vertex.
		for (map_t::value_type &v:node_map)
		{
			// Update dist[v] only if is not in sptSet, there is an edge from
			// u to v, and total weight of path from src to  v through u is
			// smaller than current value of dist[v]
			/** note that the matrix(u,v) contains the link sent by v and received by u
			 *	consequently distance from u to v is actually matrix(v,u)
			*/
			uint8_t v_receives_from_u=graph(v.first,u.first);

			if (
				(!isVisited(v.second)) &&
				(v_receives_from_u!=0) &&
				(getDist(u.second) != std::size_t(-1)) &&
				((getDist(u.second) + v_receives_from_u) < getDist(v.second))
				)
			{
				getDist(v.second) = getDist(u.second) + v_receives_from_u;
				//we update the value of prev[v] every time a new shortest path is found to v
				getPrev(v.second) = u.first;
			}
		}

	}
	//print_map(node_map);
	route=std::move(node_map);

}


Router::Router(std::size_t id):
	my_id(id),
	path_generator(id)
{

}

void Router::updateRoutes(const matrix_t &new_matrix, const buffer_state_map &buffers_state)
{
	write_lock.lock();
	if(connectivityMatrixChanged(new_matrix,buffers_state))
	{
		old_matrix=new_matrix;
		path_generator.push(old_matrix,buffers_state);
		updateRoutingTable();
	}
	write_lock.unlock();
}


bool Router::getFeasableRoute(std::list<std::size_t> &path, std::size_t source, std::size_t destination, std::size_t message_size)
{
	bool message_fits=false;
	bool debug=false;
	if(debug)
	{
		PDEBUG(std::cout,"");
	}
	if(!hasRoute(destination))
	{
		return message_fits;
	}
	write_lock.lock();

	// Check if the matrix pointer was correctly initialized
	try
	{
		if(old_matrix.size()>0)
		{

			path=findRoute(destination);

			if(!path.empty())
			{
				PDEBUG(std::cout,"Found path "<<[&path](){
					std::stringstream ss;
					for(std::size_t node:path)
					{
						ss<<+node<<"->";
					}
					return ss.str();
				}());
				path.pop_front();
				/*while ((path.size()>0) &&((message_fits=fits(source,message_size, path.size()))==false))
				{
					path.pop_back();
					PDEBUG(std::cout,"Does not fit... New path "<<[&path](){
						std::stringstream ss;
						for(std::size_t node:path)
						{
							ss<<+node<<"->";
						}
						return ss.str();
					}());
				}*/
				message_fits=fits(source,message_size, path.size());
			}
		}
	}
	catch(std::out_of_range &e)
	{
	}
	write_lock.unlock();

	return message_fits;
}

bool Router::getRoute(std::list<std::size_t> &path, std::size_t destination) const
{
	bool hasPath=false;
	if(!hasRoute(destination))
	{
		return hasPath;
	}
	write_lock.lock();

	// Check if the matrix pointer was correctly initialized
	try
	{
		if(old_matrix.size()>0)
		{

			path=findRoute(destination);

			if(!path.empty())
			{
				PDEBUG(std::cout,"Found path "<<[&path](){
					std::stringstream ss;
					for(std::size_t node:path)
					{
						ss<<+node<<"->";
					}
					return ss.str();
				}());
				path.pop_front();
				if (path.size()>0)
				{
					hasPath=true;
				}
			}
		}
	}
	catch(std::out_of_range &e)
	{
	}
	write_lock.unlock();

	return hasPath;
}

bool Router::hasRoute(std::size_t destination) const
{
	bool hasPath=false;
	write_lock.lock();

	// Check if the matrix pointer was correctly initialized
	try
	{
		if(old_matrix.size()>0)
		{
			hasPath=(routing_table.at(destination).size()>1);
		}
	}
	catch(std::out_of_range &e)
	{
		PERROR(std::cerr,"Exception here: "<<e.what());
		hasPath=false;
		//throw e;
	}
	write_lock.unlock();

	return hasPath;

}


void Router::setStartOfSlot(Router::time_point start)
{
	write_lock.lock();

	beginning_of_slot=start;
	if(old_matrix.size()>0)
	{
		slot_duration=duration((uint64_t)((double)period.count()/(double)old_matrix.size()));
	}
	else
	{
		slot_duration=period;
	}
	write_lock.unlock();
}



void Router::setPeriod(Router::milliseconds period)
{
	write_lock.lock();
	this->period=period;
	write_lock.unlock();

}

//Flag to indicate if we are using the multi-hop algorithm
bool Router::isMultihop()
{
	return MULTIHOP;
}

uint8_t Router::getMyID()
{
	return my_id;
}

std::string Router::toString() const
{
	std::stringstream ss;
	ss<<"==========Routing Table=========="<<std::endl;
	ss<<(path_generator.printPath());
	return ss.str();

}

bool Router::fits(std::size_t source, std::size_t dataSize, std::size_t number_of_hops)
{
	// NOTE:
	// Since some intermediary node may have cached the packet (because it didn't fit in the original sender's slot),
	// the function must take into account, not the original source address of the packet, but the node correspondent
	// to the current slot.
	//
	// This can be determined based on:
	//    -Current time
	//    -Beginning of this node's slot


	//uint8_t globalID=(ntohl(ip.saddr)&0x000000FF);


	uint8_t othersSlot;
	old_matrix.getOrder(source,othersSlot);

	uint8_t mySlot;
	old_matrix.getOrder(my_id,mySlot);

	uint16_t numberOfSlots=old_matrix.size();

	int slots_from_me_to_other = ( numberOfSlots - mySlot + othersSlot) % numberOfSlots;

	time_point begin_of_other_slot=beginning_of_slot+(slots_from_me_to_other)*slot_duration;
	time_point end_of_other_slot=beginning_of_slot+(slots_from_me_to_other+1)*slot_duration;

	// Note that the message might have been sent before the new round...
	return ((Clock::now()>begin_of_other_slot)&&(Clock::now()+timeToSend(dataSize, number_of_hops)<end_of_other_slot));
}


std::list<std::size_t> Router::findRoute(std::size_t dst) const
{
	return routing_table.at(dst);
}

Router::duration Router::timeToSend(std::size_t dataSize, std::size_t number_of_hops)
{

	double time = number_of_hops*T_UNICAST(dataSize+IP_UDP,DATARATE);

	duration d = std::chrono::duration_cast<duration>(doubleDuration(time));

	//std::cout<<"Time to Send: "<<time<<"us"<<std::endl;
	//std::cout<<"Time to Send: "<<d.count()<<"us"<<std::endl;

	return d;
}

bool Router::connectivityMatrixChanged(const matrix_t &matrix,const buffer_state_map &buffers) const
{

	return true;
	if( matrix.size() != old_matrix.size() )
	{
		return true;
	}

	std::list<uint8_t> new_members = matrix.getKeys();
	std::list<uint8_t> old_members = old_matrix.getKeys();

	std::pair<std::list<uint8_t>::iterator,std::list<uint8_t>::iterator> match=std::mismatch(new_members.begin(),new_members.end(), old_members.begin());
	if((match.first!=new_members.end())||(match.second!=old_members.end()))
	{
		return true;
	}

	for(uint8_t &row:new_members)
	{
		for(uint8_t &col:new_members)
		{
			if(matrix(row,col)!=old_matrix(row,col))
			{
				return true;
			}
		}
	}

	return false;
}


uint8_t Router::getNextHop(uint8_t dst)
{
	return routing_table.at(dst).front();
}

void Router::updateRoutingTable()
{
	routing_t new_routing_table;


	for(uint8_t &id: old_matrix.getKeys())
	{
		std::list<std::size_t> path;
		path_generator.get_route_to(path,id);
		if(path.front() != my_id)
		{
			path.clear();
		}
		new_routing_table.emplace_hint(new_routing_table.end(),id,path);
	}
	// Clear routing table contents
	routing_table.clear();
	routing_table=std::move(new_routing_table);

	/*std::cout<<"==========Routing Table=========="<<std::endl;
	path_generator.printPath();*/



}
