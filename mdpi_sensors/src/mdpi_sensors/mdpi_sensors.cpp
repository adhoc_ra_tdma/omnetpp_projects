#include <mdpi_sensors/mdpi_sensors.hpp>

#include <sstream>
#include <string>
#include <simulatable_sockets/simulatable_sockets.hpp>
#include <network_generics/ip_v4.hpp>

#include <simulatable_clock/simulatable_clock.hpp>


//#define debugLevel lRuntime
#ifndef debugLevel
#define debugLevel lError
#endif

#include <debug_headers/debug.hpp>

using ipv4=common::network::generics::ipv4;

static std::ofstream log_pos_matrix_buffers;
static std::mutex log_mutex_pos_matrix_buffers;


Define_Module(mdpi_sensors);

mdpi_sensors::mdpi_sensors()
{

}

mdpi_sensors::~mdpi_sensors()
{
	if(ratdma!=nullptr)
	{
		delete(ratdma);
		ratdma=nullptr;
	}

	if(buffer!=nullptr)
	{
		delete(buffer);
		buffer=nullptr;
	}
    if(matlab_timer)
	{
        matlab_timer.reset();
	}

}

void mdpi_sensors::initialize(int stage)
{
	cSimpleModule::initialize(stage);

	if (stage == 5)
	{


		std::string ifname(par("wlanDevice").stdstringValue());

        std::unique_ptr<common::network::interfaces::if_deviceI> device=common::simulation_interface::udp_sockets::new_if_device(ifname);

        std::string s=device->getAddr();
		myIP.setAddr(s);
		myID=myIP.getBytes()[3];

		uint8_t number_of_hosts=cSimulation::getActiveSimulation()->getModuleByPath("Net80211")->par("numHosts").longValue();



		/** **********************************
		 *	*	    LOGGING FOR MATLAB       *
		 *  **********************************
		 */
		log_mutex_pos_matrix_buffers.lock();
		if(myID==1)
		{
			if(!log_pos_matrix_buffers.is_open())
			{
				log_pos_matrix_buffers.open("positions_omnetpp_log.txt", std::fstream::out | std::fstream::app);
				log_pos_matrix_buffers<<"time;number_of_nodes";
				for(int i = 0;i<number_of_hosts;i++)
				{
					log_pos_matrix_buffers<<";x("<<1+i<<")"<<";y("<<1+i<<")";
				}
				for(int i = 0;i<number_of_hosts;i++)
				{
					for(int j = 0;j<number_of_hosts;j++)
					{
						log_pos_matrix_buffers<<";m("<<1+i<<","<<1+j<<")";
					}
				}
				for(int i = 0;i<number_of_hosts;i++)
				{
					log_pos_matrix_buffers<<";b("<<1+i<<")";
				}
				log_pos_matrix_buffers<<std::endl;
			}



		}

		for(size_t i=0;i<number_of_hosts;i++)
		{
			std::stringstream path;
			path<<"Net80211.host["<<i<<"]";
			cModule *module = cSimulation::getActiveSimulation()->getModuleByPath(path.str().c_str());
			perfect_coordinates.push_back(std::pair<cModule *, Coord >(module, Coord()));
			perfect_matrix.emplace(i+1,0);
			perfect_buffers.emplace(i+1,0);
		}
		getHostsPosition(perfect_coordinates);
		log_mutex_pos_matrix_buffers.unlock();
		/** **********************************
		 *	*	    LOGGING FOR MATLAB       *
		 *  **********************************
		 */


		PDEBUG(std::cout,"Openning device:"<<ifname<<std::endl<<"  IP address:"<<device.getAddr());
		/** Initialise buffer status */
		/** Setup buffers */
		for(uint8_t i=0;i<number_of_hosts;i++)
		{
			buffers_state.emplace(detail::buffer_state_map::value_type(i+1,0));
		}
		buffer=new detail::shared_buffers(myID);


		/** Setup times */
		uint64_t period_ms=par("period_ms").longValue();
		uint64_t min_ms=par("min_ms").longValue();
		uint64_t max_ms=par("max_ms").longValue();

		for(uint8_t i=0;i<number_of_hosts;i++)
		{
			state_message.push_back(std::vector<uint8_t>());
			state_message_is_dirty.push_back(true);
			state_message_sequence_number.push_back(0);
		}



		/** raTDMA functions */
		auto sender=[this](std::vector<uint8_t> &sendBuffer, const detail::buffer_state_map &buffer_state, const detail::matrix_t &conMatrix, bool &active, bool &end)
		{
			uint8_t number_of_hosts=cSimulation::getActiveSimulation()->getModuleByPath("Net80211")->par("numHosts").longValue();

			/** **********************************
			 *	*	    LOGGING FOR MATLAB       *
			 *  **********************************
			 */
			log_mutex_pos_matrix_buffers.lock();

			/** setup perfect positions */
			getHostsPosition(perfect_coordinates);

			/** setup perfect matrix */
			// clear my line and get lines from others
			for(int i=0;i<number_of_hosts;i++)
			{
				std::stringstream path;
				path<<"Net80211.host["<<i<<"].udpApp[0].udpManagerApp[0]";
				mdpi_sensors *module = dynamic_cast<mdpi_sensors *>( cSimulation::getActiveSimulation()->getModuleByPath(path.str().c_str()));
				// get line from node i+1
				if((i+1)!=myID)
				{
					for(int j=0;j<number_of_hosts;j++)
					{
						perfect_matrix(i+1,j+1)=module->perfect_matrix(i+1,j+1);
					}
				}
				perfect_matrix(myID,i+1)=0;
			}
			// fill my line
			for(detail::matrix_t::key_type &key:conMatrix.getKeys())
			{
				// update my line
				perfect_matrix(myID,key)=conMatrix(myID,key);
			}

			/** setup perfect buffers */
			// set my line
			perfect_buffers(myID)=buffer_state(myID);
			// get other buffers
			for(int i=0;i<number_of_hosts;i++)
			{
				if((i+1)!=myID)
				{
					std::stringstream path;
					path<<"Net80211.host["<<i<<"].udpApp[0].udpManagerApp[0]";
					mdpi_sensors *module = dynamic_cast<mdpi_sensors *>( cSimulation::getActiveSimulation()->getModuleByPath(path.str().c_str()));
					perfect_buffers(i+1)=module->perfect_buffers(i+1);
				}
			}




			if(myID==1)
			{

				getHostsPosition(perfect_coordinates);
				log_pos_matrix_buffers<<SIMTIME_DBL(simTime())<<";"<<+number_of_hosts;

				for(std::pair<cModule *, Coord > &pair: perfect_coordinates)
				{
					log_pos_matrix_buffers<<";"<<pair.second.x<<";"<<pair.second.y;
				}

				for(size_t i=0;i<number_of_hosts;i++)
				{
					for(size_t j=0;j<number_of_hosts;j++)
					{
						log_pos_matrix_buffers<<";"<<+perfect_matrix(i+1,j+1);
					}
				}

				for(size_t i=0;i<number_of_hosts;i++)
				{
					log_pos_matrix_buffers<<";"<<+perfect_buffers(i+1);
				}

				log_pos_matrix_buffers<<std::endl;


			}
			log_mutex_pos_matrix_buffers.unlock();
			/** **********************************
			 *	*	    LOGGING FOR MATLAB       *
			 *  **********************************
			 */

			if(myID==1) for(int i=0;i<10;i++) PRUNTIME(std::cout, std::endl);
			PRUNTIME(std::cout, "**************************************************" <<std::endl);
			PRUNTIME(std::cout, "**************************************************" <<std::endl);
			PRUNTIME(std::cout, "**\t\tNode "<<+myID<<"	Sending cycle\t\t**" <<std::endl);
			PRUNTIME(std::cout, "**************************************************" <<std::endl);

			/** Copy to send to matlab */
			for(const detail::buffer_state_map::value_type &node: buffer_state)
			{
				buffers_state(node.first)=node.second;
			}

			active=this->active;
			end=this->end;

			common::serialisation::Container c;
			for(uint8_t i=0;i<number_of_hosts;i++)
			{
				common::serialisation::serialise(c,state_message_sequence_number[i]);
				common::serialisation::serialise(c,state_message[i]);
			}
			sendBuffer.insert(sendBuffer.end(),c.begin(),c.end());
//			std::cout<<"node "<<+this->myID<<" sending state"<<std::endl;

			return;
		};



		auto receiver=[this](const message &msg, const detail::buffer_state_map &buffer_state)
		{
//			std::cout<<"node "<<+this->myID<<" received state"<<std::endl;
			/** Copy to send to matlab */
			for(const detail::buffer_state_map::value_type &node: buffer_state)
			{
				buffers_state(node.first)=node.second;
			}
			common::serialisation::Container c(msg.getData().begin(),msg.getData().end());

			uint64_t new_state_message_sequence_number;
			std::vector<uint8_t> new_state_message;
			uint8_t number_of_hosts=cSimulation::getActiveSimulation()->getModuleByPath("Net80211")->par("numHosts").longValue();
			for( uint8_t i = 0 ; i < number_of_hosts ; i++ )
			{
				new_state_message.clear();
				common::serialisation::deserialise(c,new_state_message_sequence_number);
				common::serialisation::deserialise(c,new_state_message);
				if(new_state_message_sequence_number>state_message_sequence_number[i])
				{
					state_message_is_dirty[i]=true;
					state_message_sequence_number[i]=new_state_message_sequence_number;
					state_message[i].clear();
					state_message[i]=new_state_message;
//					std::cout<<"node "<<+this->myID<<" received new state from node "<<+i+1<<std::endl;

				}
			}


		};

		auto unicast_receiver=[this](const message &msg)
		{
			std::chrono::high_resolution_clock::time_point time_start=std::chrono::high_resolution_clock::now();

			PRUNTIME(std::cout, "**************************************************"<<std::endl);
			PRUNTIME(std::cout, "**\t\tNode "<<+myID<<"	Receiving\t\t**"<<std::endl);
			PRUNTIME(std::cout, "**************************************************"<<std::endl);

			/* Matlab oracle */
			// No data to be sent
			std::vector<uint8_t> data(msg.getData().begin(),msg.getData().end());
			send_to_matlab(data);

			//	PDEBUG(std::cout,std::boolalpha<<"Trying to push to buffers: succeess "<<buffers[id-1]->push_message(data));
			//matlab_socket->
			while(!receive_from_matlab());

			/* Matlab oracle */
			std::chrono::high_resolution_clock::time_point time_end=std::chrono::high_resolution_clock::now();
			//std::cout<<"Unicast: "<<(time_end-time_start).count()<<std::endl;

		};


        ratdma = new communications::raTDMA::raTDMA(*device,period_ms,min_ms,max_ms,sender, receiver, unicast_receiver);

		/** Matlab */
		std::function<void()> matlab_timer_cb=[&]()
		{


			/* Matlab oracle */

			uint8_t number_of_hosts=cSimulation::getActiveSimulation()->getModuleByPath("Net80211")->par("numHosts").longValue();
			for(uint8_t i=0;i<number_of_hosts;i++)
			{
				if(state_message_is_dirty[i])
				{
//					std::cout<<"node "<<+this->myID<<" sending a new state from node "<<+i+1<<std::endl;

					state_message_is_dirty[i]=false;
					send_to_matlab(state_message[i]);

					//	PDEBUG(std::cout,std::boolalpha<<"Trying to push to buffers: succeess "<<buffers[id-1]->push_message(data));
					while(!receive_from_matlab());
				}
			}


		};

		matlab_socket=new udp_future_unicast_socket(50001+myIP.getBytes()[3]);
		std::chrono::nanoseconds matlab_sync_period(std::chrono::milliseconds(150));
		std::cout<<"Timer period: "<<matlab_sync_period.count()<<std::endl;

        matlab_timer=common::simulation_interface::timer::new_timer();
		matlab_timer->start(matlab_timer_cb,matlab_sync_period,matlab_sync_period);
		std::cout<<"Timer first firing at: "<<matlab_timer->absoluteExpiryTime().count()<<std::endl;


        if(device)
        {
            device.reset();
        }
	}



}


void mdpi_sensors::finish()
{
	// Do something
	if(ratdma!=nullptr)
	{
		delete(ratdma);
		ratdma=nullptr;
	}

	if(buffer!=nullptr)
	{
		delete(buffer);
		buffer=nullptr;
	}

    if(matlab_timer)
    {
        matlab_timer.reset();
    }

	/** **********************************
			 *	*	    LOGGING FOR MATLAB       *
			 *  **********************************
			 */

	log_mutex_pos_matrix_buffers.lock();
	if(myID==1)
	{
		if(log_pos_matrix_buffers.is_open())
		{
			log_pos_matrix_buffers.close();
		}

	}
	log_mutex_pos_matrix_buffers.unlock();
	/** **********************************
			 *	*	    LOGGING FOR MATLAB       *
			 *  **********************************
			 */

	cSimpleModule::finish();
}

void mdpi_sensors::handleMessage(cMessage *msg)
{

}


void mdpi_sensors::setHostPosition(const std::pair<cModule * , Coord> &pair)
{
	// Create the path to the given host
	cModule *module=pair.first;
	const Coord &coord=pair.second;
	if (module!=nullptr)
	{
		//Requires #include mobilityAcess(.h) to access the module modified by Daniel Ramos to "instantmobility"
		//accesses instant mobility module and calls setinstantposition
		InstantMobility *im=dynamic_cast<InstantMobility *>(MobilityAccess().get(module));
		if(im!=nullptr)
		{
			im->setInstantPosition(coord);
		}
		else
		{
			cRuntimeError((std::string("Cannot find instant mobility in host")).c_str());
		}
	}
	else
	{
		cRuntimeError((std::string("Cannot find host to move node")).c_str());

	}
}

void mdpi_sensors::setHostsPosition(const std::vector< std::pair<  cModule * ,  Coord>> &pairs)
{
	// Function to set the position of one node
	for (const std::pair< cModule * , Coord > &pair:pairs)
	{
		setHostPosition(pair);
	}

}



void mdpi_sensors::getHostPosition(std::pair<cModule *, Coord> &pair)
{
	Coord coord;
	cModule *module=pair.first;
	if (module!=nullptr)
	{
		InstantMobility * im=dynamic_cast<InstantMobility *>(MobilityAccess().get(const_cast<cModule *>(module)));
		if(im!=nullptr)
		{
			im->getInstantPosition(coord);
		}
		else
		{
			cRuntimeError((std::string("Cannot find instant mobility in host")).c_str());
		}
	}
	else
	{
		cRuntimeError((std::string("Invalid module")).c_str());
	}
	pair.second=coord;

}


void mdpi_sensors::getHostsPosition(std::vector<std::pair< cModule *, Coord>> &pairs)
{
	for(std::pair< cModule *, Coord> &pair: pairs)
	{
		getHostPosition(pair);
	}
}

void mdpi_sensors::send_to_matlab(std::vector<uint8_t> &packet_to_matlab)
{
	common::serialisation::Container synchronisation_data_to_matlab;

	common::serialisation::serialise(synchronisation_data_to_matlab,myID);

	double current_time=SIMTIME_DBL(simTime());
	common::serialisation::serialise(synchronisation_data_to_matlab,current_time);



	for(detail::buffer_state_map::value_type &buff:buffers_state)
	{
		uint32_t buffer_size=buff.second;
		common::serialisation::serialise(synchronisation_data_to_matlab,buffer_size);
	}

	uint16_t payload_size=packet_to_matlab.size();		//(2 bytes)
	common::serialisation::serialise(synchronisation_data_to_matlab,payload_size);

	if(payload_size>0)
	{
		for(uint8_t &datum: packet_to_matlab)
		{
			common::serialisation::serialise(synchronisation_data_to_matlab,datum);
		}
	}


	message msg;
	message::pType data(synchronisation_data_to_matlab.begin(),synchronisation_data_to_matlab.end());
	msg.setData(data);
	msg.setUDPEndpoint(endpoint("127.0.0.1",50001));
	matlab_socket->future_write(msg).get();



	PRUNTIME(std::cout,current_time<<"ms: Sent "<<synchronisation_data_to_matlab.size()<<" Bytes to Matlab with "<<payload_size<<"Bytes of payload."<<std::endl);
	PRUNTIME(std::cout,"\tBuffers:");
	for(detail::buffer_state_map::value_type &buff:buffers_state)
	{
		PRUNTIME(std::cout,+buff.second<<" ");
	}
	PRUNTIME(std::cout,std::endl);


}

bool mdpi_sensors::receive_from_matlab()
{

	//Sync with matlab
	std::future<message> matlab_msg=matlab_socket->future_read();
	message m=matlab_msg.get();
	message::pType &msg_data=m.getData();


	common::serialisation::Container data_from_matlab(msg_data.begin(),msg_data.end());

	uint8_t msg_type;
	common::serialisation::deserialise(data_from_matlab, msg_type);
	PDEBUG(std::cout,"Received "<<msg_data.size()
		   <<" Bytes of a message of type "<<+msg_type);
	if(msg_type!=0)
	{
		if(msg_type==3)
		{
			PRUNTIME(std::cout,"Finishing..."<<std::endl);
			end=true;
			return true;
		}
		cRuntimeError("An out-of-order message was received from matlab, ignoring.");
		return false;
	}

	uint8_t node_id;
	common::serialisation::deserialise(data_from_matlab, node_id);

	double matlab_time;
	common::serialisation::deserialise(data_from_matlab, matlab_time);

	uint8_t active;
	common::serialisation::deserialise(data_from_matlab, active);
	this->active=(active!=0);
	PRUNTIME(std::cout,"Node status: "<<(active?"activated":"deactivated")<<std::endl);
	std::vector<std::pair<cModule *, Coord >> new_positions;

	uint8_t number_of_hosts=cSimulation::getActiveSimulation()->getModuleByPath("Net80211")->par("numHosts").longValue();
	for(size_t i=0;i<number_of_hosts;i++)
	{
		std::stringstream path;
		path<<"Net80211.host["<<i<<"]";
		cModule *module = cSimulation::getActiveSimulation()->getModuleByPath(path.str().c_str());
		new_positions.push_back(std::pair<cModule *, Coord >(module, Coord()));
	}
	getHostsPosition(new_positions);
	uint16_t node=1;
	PRUNTIME(std::cout,"Nodes positions"<<std::endl);
	for(std::pair<cModule *, Coord > &pair: new_positions)
	{
		float x,y;
		common::serialisation::deserialise(data_from_matlab, x);
		common::serialisation::deserialise(data_from_matlab, y);
		pair.second.x=x;
		pair.second.y=-y;
		PRUNTIME(std::cout,"\tNode "<<node++<<": ("<<x<<","<<y<<")"<<std::endl);
	}

	setHostsPosition(new_positions);

	uint8_t number_of_messages;
	common::serialisation::deserialise(data_from_matlab, number_of_messages);
	PRUNTIME(std::cout,"Pushing "<<+number_of_messages<<" new messages to Tx queues"<<std::endl);

	PDEBUG(std::cout,"Expecting "<<+number_of_messages<<" messages");

	for(std::size_t i=0;i<number_of_messages;i++)
	{
		std::shared_future<message> matlab_future=matlab_socket->future_read();
		message matlab_msg=matlab_future.get();
		common::serialisation::Container serialised_msg(matlab_msg.getData().begin(),matlab_msg.getData().end());

		DEBUG_FUNCTION
		{
			for(uint8_t &datum: serialised_msg)
			{
				std::cout<<std::hex<<+datum<<std::dec<<" ";
			}
		};

		uint8_t type;
		common::serialisation::deserialise(serialised_msg, type);

		if(type!=1)
		{
			cRuntimeError("An out-of-order message was received from matlab, ignoring.");
			continue;
		}

		uint8_t destination;
		common::serialisation::deserialise(serialised_msg, destination);
		uint16_t payload_size;
		common::serialisation::deserialise(serialised_msg, payload_size);

		PDEBUG(std::cout,"Received "<<matlab_msg.getData().size()<<" Bytes of a message of type "<<+type
			   <<" destination is "<<+destination
			   <<" and contains "<<+payload_size<<"Bytes:");

		if(destination!=255)
		{
			detail::shared_buffers::message_t data;
			for(std::size_t j=0;j<payload_size;j++)
			{
				data.msg[j]=serialised_msg[j];
			}
			data.destination=destination;
			data.size=payload_size;
			buffer->push_message(data);
		}
		else
		{
			state_message[myID-1].clear();
			for(std::size_t j=0;j<payload_size;j++)
			{
				state_message[myID-1].push_back(serialised_msg[j]);
			}
			state_message_sequence_number[myID-1]++;
		}
	}
	return true;
}







/*void omnetpp_to_matlab::serialise(detail::Container &ret) const
{

	common::serialisation::serialise(ret,id);
	common::serialisation::serialise(ret,current_time);

	//for(std::pair<const uint8_t, detail::buffer_manager> &buff:buffers_state)
	//{
	//	uint32_t buffer_size=buff.second.get().first;
	//	common::serialisation::serialise(ret,buffer_size);
	//}

	uint16_t payload_size=packet_to_matlab.size();		//(2 bytes)
	common::serialisation::serialise(ret,payload_size);

	if(payload_size>0)
	{
		for(uint8_t &datum: packet_to_matlab)
		{
			common::serialisation::serialise(ret,datum);
		}
	}

}

void omnetpp_to_matlab::deserialise(detail::Container &ret)
{

}*/
