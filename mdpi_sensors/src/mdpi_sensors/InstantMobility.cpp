#include <mdpi_sensors/InstantMobility.hpp>
#include "FWMath.h"


Define_Module(InstantMobility);


InstantMobility::InstantMobility()
{
	speed = 0;
}

void InstantMobility::initialize(int stage)
{
	LineSegmentsMobilityBase::initialize(stage);

	EV_TRACE << "initializing InstantMobility stage " << stage << endl;
	if (stage == 0)
	{
		speed = par("speed");
		stationary = speed == 0;
	}
}

void InstantMobility::setTargetPosition()
{
	//targetPosition = getRandomPosition();
	//Coord positionDelta = targetPosition - lastPosition;
	//double distance = positionDelta.length();
  //  nextChange = simTime() + distance / speed;
}



void InstantMobility::setInstantPosition(Coord newpos)
{
	//Atualiza a posi����o
	lastPosition = newpos;
	//Atualiza variavel de quando foi atualizada a posi����o
	lastUpdate = simTime();
	//Comandos para atualizar a representa����o visual na IDE
	LineSegmentsMobilityBase::MovingMobilityBase::emitMobilityStateChangedSignal();
	LineSegmentsMobilityBase::MovingMobilityBase::updateVisualRepresentation();



}


void InstantMobility::getInstantPosition(Coord &newpos)
{
	newpos=Coord(lastPosition);
}
