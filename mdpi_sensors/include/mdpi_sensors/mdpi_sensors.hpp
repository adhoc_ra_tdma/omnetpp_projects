#ifndef __MDPI_SENSORS_HPP__
#define __MDPI_SENSORS_HPP__

#include <base/INETDefs.h>

#include <applications/common/ApplicationBase.h>


#include <ratdma/raTDMA.hpp>

#include <mdpi_sensors/InstantMobility.hpp>

#include <mobility/common/MobilityAccess.h>

#include <vector>
#include <routing/routing.hpp>


#include <linux_sockets/udp_unicast_socket.hpp>
#include <linux_sockets/udp_future_sockets.hpp>

#include <simulatable_sockets/simulatable_sockets.hpp>
#include <simulatable_timer/simulatable_timer.hpp>

namespace detail
{
using udp_endpoint=common::network::generics::udp_endpoint;
using MessageUDPI=common::network::generics::MessageUDPI;
using ipv4=common::network::generics::ipv4;

using udp_unicast_socket=common::network::linux_implementation::sockets::udp_unicast_socket_implementation;
using udp_future_unicast_socket=common::network::linux_implementation::sockets::udp_future_socket<udp_unicast_socket, uint16_t>;

using time_point = communications::raTDMA::time_point;
using message = communications::raTDMA::udp_message;
using matrix_t = communications::raTDMA::matrix_t;
using endpoint = communications::raTDMA::udp_endpoint_t;

using Container=common::serialisation::Container;

using buffer_state_map=communications::raTDMA::buffer_state_map;
using shared_buffers=communications::raTDMA::shared_buffers;
}
/*
class omnetpp_to_matlab: public common::serialisation::SerialisableI
{
	public:
		uint8_t &id;
		double &current_time;
		//std::map<uint8_t,detail::buffer_manager> &buffers_state;
		uint16_t &payload_size;
		std::vector<uint8_t> &packet_to_matlab;


		virtual void serialise( detail::Container &ret) const;
		virtual void deserialise(detail::Container &ret);
};
*/





class INET_API mdpi_sensors : public cSimpleModule
{
		using udp_endpoint=detail::udp_endpoint;
		using MessageUDPI=detail::MessageUDPI;
		using ipv4=detail::ipv4;

		using udp_unicast_socket=detail::udp_unicast_socket;
		using udp_future_unicast_socket=detail::udp_future_unicast_socket;

		using time_point = detail::time_point;
		using message = detail::message;
		using matrix_t = detail::matrix_t;
		using endpoint = detail::endpoint;

		using sim_clock=typename common::simulation_interface::simulation_clock;
		using duration=typename sim_clock::duration;
	protected:

		simtime_t startTime;
		simtime_t stopTime;

		uint8_t myID;
		ipv4 myIP;


	public:
		mdpi_sensors();
		~mdpi_sensors();

		virtual int numInitStages() const  {return 6;}


		void setHostPosition(const std::pair<cModule *, Coord> &pair);
		void setHostsPosition(const std::vector<std::pair< cModule *, Coord>> &pairs);

		void getHostPosition( std::pair<cModule *, Coord> &pair);
		void getHostsPosition(std::vector<std::pair< cModule *, Coord>> &pairs);

	protected:

		void send_to_matlab(std::vector<uint8_t> &packet_to_matlab);
		bool receive_from_matlab();

		virtual void handleMessage(cMessage *msg);
		virtual void initialize(int stage);
		virtual void finish();

		communications::raTDMA::raTDMA *ratdma;




		/** for matlab */
        std::unique_ptr<common::time::interfaces::TimerI> matlab_timer=nullptr;
		udp_future_unicast_socket *matlab_socket;

		detail::buffer_state_map buffers_state;
		detail::shared_buffers * buffer=nullptr;
		bool active=true;
		bool end=false;
		std::vector<std::vector<uint8_t>> state_message;
		std::vector<bool> state_message_is_dirty;
		std::vector<uint64_t> state_message_sequence_number;


		/* Global knowledge */
		detail::matrix_t perfect_matrix;
		detail::buffer_state_map perfect_buffers;
		std::vector<std::pair< cModule *, Coord>> perfect_coordinates;


};


#endif
