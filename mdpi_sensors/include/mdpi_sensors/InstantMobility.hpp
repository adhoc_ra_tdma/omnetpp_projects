/****************************************************************************
 *									BASED ON								*
 ****************************************************************************

 ****************************************************************************
 *  -*- mode:c++ -*- ********************************************************
 * file:        ConstSpeedMobility.h
 *
 * author:      Steffen Sroka
 *
 * copyright:   (C) 2004 Telecommunication Networks Group (TKN) at
 *              Technische Universitaet Berlin, Germany.
 *
 *              This program is free software; you can redistribute it
 *              and/or modify it under the terms of the GNU General Public
 *              License as published by the Free Software Foundation; either
 *              version 2 of the License, or (at your option) any later
 *              version.
 *              For further information see file COPYING
 *              in the top level directory
 ***************************************************************************
 * part of:     framework implementation developed by tkn
 **************************************************************************

 ***************************************************************************
 * Modified by:     Daniel Ramos ; Luis Oliveira
 **************************************************************************/


#ifndef INSTANT_MOBILITY_H
#define INSTANT_MOBILITY_H

#include "INETDefs.h"

#include "LineSegmentsMobilityBase.h"


class INET_API InstantMobility : public LineSegmentsMobilityBase
{
  protected:
	/** @brief Speed parameter. */
	double speed;

  protected:
	virtual int numInitStages() const { return 3; }

	/** @brief Initializes mobility model parameters. */
	virtual void initialize(int stage);

	/** @brief Calculate a new target position to move to. */
	virtual void setTargetPosition();

  public:
	InstantMobility();
	virtual void setInstantPosition(Coord newpos);
	virtual void getInstantPosition(Coord &newpos);


};

#endif
