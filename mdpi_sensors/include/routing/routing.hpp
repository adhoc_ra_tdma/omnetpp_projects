#ifndef __ROUTING_HPP__
#define __ROUTING_HPP__

#include <iostream>

#include <string>

#include <vector>
#include <array>
#include <map>

#include <ratdma/ratdma_properties.hpp>
#include <ratdma/net_calc.hpp>
#define MULTIHOP true




#define getDist(tuple) std::get<0>(tuple)
#define getBuffer(tuple) std::get<1>(tuple)
#define getPrev(tuple) std::get<2>(tuple)
#define isVisited(tuple) std::get<3>(tuple)

class dijkstra
{
	private:
		/** Distance, buffer, previous hop, visited */
		using node_t=std::tuple<std::size_t, std::size_t, std::size_t, bool>;
		using map_t=std::map<std::size_t, node_t>;

	public:

		using matrix_t=communications::raTDMA::matrix_t;
		using buffer_state_map=communications::raTDMA::buffer_state_map;

		dijkstra(std::size_t myID);

		dijkstra()=delete;

		dijkstra(const dijkstra &other)=default;
		dijkstra &operator=(const dijkstra &other)=default;

		dijkstra(dijkstra &&other)=default;
		dijkstra &operator=(dijkstra &&other)=default;

		virtual ~dijkstra()=default;

		void push(const matrix_t &matrix, const buffer_state_map &buffers);

		void get_route_to(std::list<std::size_t> &route_to_dest, std::size_t dest) const;

		std::string printPath() const;

	protected:

		// An utility function to find the vertex with minimum distance value, from
		// the set of vertices not yet included in shortest path tree
		map_t::value_type &minDistance(map_t &nodes);

		//An utility function to print the minimum path by recursively printing the previous node, from the destination to the source

		// Funtion that implements Dijkstra's single source shortest path algorithm
		// for a graph represented using adjacency matrix representation
		void solve_dijkstra(const matrix_t &graph, const buffer_state_map &buffers, int src);


	private:
	/** The output map. node_map.at(id) will hold:
	 *
	 *	get<0> = dist:
	 *		The shortest distance from src to id
	 *
	 *	get<1> = prev:
	 *		For every node id other than the source, remember not only the
	 *		distance to it, but also the previous node in the path to it
	 *
	 *	get<2> = visited
	 *		For every node id, if vertex id is included in shortest path tree
	 *		or shortest distance from src to id is finalized
	 */
		map_t route;
		std::size_t my_id;
};

#include <simulatable_clock/simulatable_clock.hpp>
class Router
{

	private:
		using Clock = common::simulation_interface::simulation_clock;
		using time_point = Clock::time_point;
		using duration = Clock::duration;
		using milliseconds = std::chrono::milliseconds;
		using doubleDuration = std::chrono::duration<double>;


		//Copy of the connectivity matrix

		using routing_t=std::map<std::size_t,std::list<std::size_t>>;

	public:
		using matrix_t=communications::raTDMA::matrix_t;
		using buffer_state_map=communications::raTDMA::buffer_state_map;

		Router(std::size_t id);

		void updateRoutes(const matrix_t &new_matrix, const buffer_state_map &buffers_state );
		bool getFeasableRoute(std::list<std::size_t> &path, std::size_t source, std::size_t destination, std::size_t message_size);
		bool getRoute(std::list<std::size_t> &path, std::size_t destination) const;

		bool hasRoute(std::size_t destination) const;

		void setStartOfSlot(time_point start);
		void setPeriod(milliseconds period);

		//Estimated transmission time for a given data size
		duration timeToSend(std::size_t dataSize, std::size_t number_of_hops);

		bool isMultihop();

		uint8_t getMyID();

		std::string toString() const;

	protected:

		std::size_t my_id;
		// Routing Table
		routing_t routing_table;
		dijkstra path_generator;
		matrix_t old_matrix;
		buffer_state_map buffers_state;

		bool fits(std::size_t source, std::size_t dataSize,std::size_t number_of_hops);
		std::list<std::size_t> findRoute(std::size_t dst) const;

		//inline common::Connectivity::ConnectivityMatrixReader *getMatrix();
		//std::map<uint8_t,std::array<uint8_t,ETH_ALEN>> mapOfIDs;

		time_point beginning_of_slot=Clock::now();   //Beginning time of the current node slot
		duration slot_duration=duration::zero();     //period of each slot
		duration period=duration::zero();            //Period of each RATDMA round

		//Routing auxiliary functions
		bool connectivityMatrixChanged(const matrix_t &matrix, const buffer_state_map &buffers_state) const;
		uint8_t getNextHop(uint8_t dst);
		void updateRoutingTable();

		std::atomic_size_t reading_access;
		std::atomic_bool writting;
		mutable std::mutex write_lock;


};

#endif
