#ifndef __UNICAST_SENDER_HPP__
#define __UNICAST_SENDER_HPP__

#include <simulatable_clock/simulatable_clock.hpp>
#include <network_generics/udp_message.hpp>
#include <routing/routing.hpp>
#include <shared_buffers/shared_buffers.hpp>

#include <ratdma/ratdma_properties.hpp>
#include <ratdma/net_calc.hpp>
class UnicastSender
{
	private:
		using upd_message = common::network::generics::MessageUDP;
		using time_point = common::simulation_interface::simulation_clock::time_point;
		using matrix_t = communications::raTDMA::matrix_t;
		using buffer_state_map = communications::raTDMA::buffer_state_map;
		using duration = communications::raTDMA::duration;
		using duration_double = communications::raTDMA::duration_double;
		using unicast_socket_interface = communications::raTDMA::unicast_socket_interface;
		using shared_buffers = common::buffers::shared_buffers;
		using timer_interface = communications::raTDMA::timer_i;
		using udp_endpoint = communications::raTDMA::udp_endpoint_t;
		using ipv4_address = communications::raTDMA::ipv4_address;
		using milliseconds=std::chrono::milliseconds;
	public:

		UnicastSender()=delete;
		UnicastSender(ipv4_address addr, milliseconds period, uint16_t port, std::function<void(const upd_message &)> userUnicastHandler);

		bool start_slot(const upd_message &msg, const time_point &start_of_slot, const matrix_t & connectivity_matrix, const buffer_state_map &buffer_state);

		void update_table(const matrix_t &new_connectivity_matrix, const buffer_state_map &buffer_state);

		std::string get_routing_string() const;

		const Router &getRouter() const;
	protected:

		void process_next_packet();


		bool get_next_routable_packet(shared_buffers::message_t &packet);

		ipv4_address ip;

		std::unique_ptr<unicast_socket_interface> socket;
		shared_buffers buffer;
		std::unique_ptr<timer_interface> next_send;

		uint16_t port;

		std::function<void(const upd_message &)> userUnicastHandler;

		Router r;
};

#endif
