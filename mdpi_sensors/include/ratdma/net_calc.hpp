#ifndef __NET_CALC_HPP__
#define __NET_CALC_HPP__


#define DATARATE (2000000.0)
#define BROADCAST_DATARATE (1000000.0)

#define T_PAYLOAD(d,rate) (((d)*8.0)/(rate))
#define T_DIFS 0.000028
#define T_SIFS 0.000010
#define T_BACKOFF (0.000135)
#define T_PREAMBLE 0.000192
#define T_ACK ((T_SIFS)+(T_PREAMBLE)+(T_PAYLOAD(14,1000000)))


#define IP_UDP (28)
#define IEEE80211 (40)
#define T_FRAME(d,rate) (T_DIFS+T_BACKOFF+T_PREAMBLE+T_PAYLOAD(d+IEEE80211,rate))
#define T_UNICAST(d,rate) (T_FRAME(d,rate)+T_ACK)
#define T_BROADCAST(d,rate) (T_FRAME(d,rate))
#define T_MULTICAST(d,rate) (T_BROADCAST(d,rate))

#endif
