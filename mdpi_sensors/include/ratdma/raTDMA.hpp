#ifndef RATDMA_HPP
#define RATDMA_HPP

#define SYNCHRONISED
//#define WAIT_FOR_ONE

#include <functional>

#include <logging/logging.hpp>



#include <ratdma/net_calc.hpp>

#include<ratdma/unicast_sender.hpp>

#include <ratdma/ratdma_properties.hpp>
#include <vector>





namespace communications
{
namespace raTDMA
{



class raTDMA
{
	private:
	public:

		raTDMA(network_device_i &dev,
			   uint64_t setPeriod,
			   uint64_t min,
			   uint64_t max,
			   std::function<void(std::vector<uint8_t> &, const buffer_state_map &, const matrix_t &matrix, bool &active, bool &end)> sendFunction,
			   std::function<void(const udp_message &, const buffer_state_map &buffer_state)> rcvFunction,
			   std::function<void(const udp_message &)> userUnicastHandler);

		~raTDMA();


	protected:


		//void cleanup();
		void synchronise(ipv4_address receivedAddress, time_point receptionTime);
		void fired();


		void sendDone(std::size_t bytes_transferred);


		void receiveDone(const udp_message &msg);

	private:


		ipv4_address myAddress;
		/* raTDMA config */
		time_point current_cycle_start;
		nanoseconds period;
		nanoseconds minDelay;
		nanoseconds maxDelay;
		nanoseconds periodDelay;
		/* */

		bool configured;

#ifdef WAIT_FOR_ONE
		boost::interprocess::interprocess_semaphore lock;
#endif

		std::mutex sending;
		connectivity_matrix conMatrix;

		std::unique_ptr<common::simulation_interface::counter::counter_interface> sendingTimeCounter=common::simulation_interface::counter::new_counter();
		common::Logging::LogStream sendingTimeLog;

		/* Socket and Timer Services */

		std::function<void(std::vector<uint8_t> &, const buffer_state_map &, const matrix_t &matrix, bool &active, bool &end)> userTimerHandler;
		std::function<void(const udp_message &, const buffer_state_map &buffer_state)> userReceiveDone;

		std::vector<uint8_t> receiveBuffer;
		std::vector<uint8_t> sendBuffer;

		std::unique_ptr<timer_i> sendTimer;
		std::unique_ptr<multicast_socket_interface> mcast;

		UnicastSender unicast_sender;

};
}
}

#endif
