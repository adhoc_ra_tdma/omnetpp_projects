#ifndef __RATDMA_PROPERTIES_HPP__
#define __RATDMA_PROPERTIES_HPP__

#include <simulatable_clock/simulatable_clock.hpp>
#include <simulatable_sockets/simulatable_sockets.hpp>
#include <simulatable_timer/simulatable_timer.hpp>
#include <relative_localisation/connectivity_matrix.hpp>
//#include <shared_buffers/shared_buffers.hpp>

#define MULTICAST_IP	"224.16.32.39"
#define MULTICAST_PORT	50000U
#define UNICAST_PORT	20000U

namespace communications
{
namespace raTDMA
{



using timer_i = common::simulation_interface::timer::timer_interface;
using unicast_socket_interface = common::simulation_interface::udp_sockets::unicast_socket_interface;
using multicast_socket_interface = common::simulation_interface::udp_sockets::multicast_socket_interface;
using network_device_i = common::network::interfaces::if_deviceI;
using udp_endpoint_t = common::network::generics::udp_endpoint;
using udp_endpoint_i = common::network::interfaces::udp_endpointI;
using udp_message = common::network::generics::MessageUDP;
using ipv4_address=common::network::generics::ipv4;

using connectivity_matrix = common::relative_localisation::ConnectivityMatrix<uint8_t,uint8_t, uint8_t >;
using matrix_t = typename connectivity_matrix::connectivity_matrix_t;

using shared_buffers = common::relative_localisation::shared_buffers;
using buffer_state_map=connectivity_matrix::buffer_state_t;


using duration_double = std::chrono::duration<double>;
using milliseconds = std::chrono::milliseconds;
using nanoseconds = std::chrono::nanoseconds;
using clock_t = common::simulation_interface::simulation_clock;
using time_point = clock_t::time_point;
using duration = clock_t::duration;




}
}



#endif

